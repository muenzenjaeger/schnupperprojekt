using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


// !! ALS STARTDATEI "Schnupperspiel.csproj - Debug|AnyCPU" auswählen !!

namespace Schnupperspiel{

    public partial class frmGame : Form{

        //Auf die andere Klasse zugreifen
        public Game game = new Game();

        //Zufällig
        private Random random = new Random();

        //Variablen deklarieren für Position von Spieler
        public static int xPlayer;
        public static int yPlayer;

        //Variablen deklarieren für Position von Münzen
        private int xCoin;  
        private int yCoin;

        //Konstruktor: Kompontenten hinzufügen
        public frmGame() {
            InitializeComponent();
        }


        //Ganzes Spielfeld wird hier aufgebaut
        private void loadGame(object sender, EventArgs e){ 
            Timer tmrGame = game.tmrGame;

            tmrGame.Tick += new System.EventHandler(this.tmrGame_Tick);
            Timer tmrCoin = game.tmrCoin;
            tmrCoin.Tick += new System.EventHandler(this.tmrCoin_Tick);
            Timer tmrEnemy = game.tmrEnemy;
            tmrEnemy.Tick += new System.EventHandler(this.tmrEnemy_Tick);
            Timer tmrSpeed = game.tmrSpeed;
            tmrSpeed.Tick += new System.EventHandler(this.tmrSpeed_Tick);
            game.makeGame(this);
            
            //Form und Panel
           


            //Startknopf
            

            //Stopknopf


            //Speedknopf


            //Zeitlabel


            //Punktelabel


            //Höchstpunktezahllabel


            //Namelabel


            //Zeittext


            //Punktetext


            //Höchstpunktezahltext

            
            //Interval Spieltimer


            //Interval Münzentimer


            //Spieler


            //Spielersteuerung


            //Interval Gegnertimer


            //Wand
                  
        }     

        //Spieltimer
        private void tmrGame_Tick(object sender, EventArgs e){

        }

        //Münzentimer
        private void tmrCoin_Tick(object sender, EventArgs e){
            
        }
        private void tmrEnemy_Tick(object sender, EventArgs e){

        }
        private void tmrSpeed_Tick(object sender, EventArgs e){  
            
        }

        private void createCoin(){
      
        }
    }
}
